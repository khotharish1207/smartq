import styled from "styled-components";
import { Paper } from "material-ui";

export const CartContainer = styled(Paper)`
  width: 23%;
  height: 300px;
  float: left;
  background: yellow;
`;

export const SelectedFoodItem = styled.div`
  border-bottom: 1px solid;
  margin: 25px 10% 0;
`;

export const Total = styled.div`
  margin: 5px 10%;
`;

export const Price = styled.span`
  float: right;
`;

export const Count = styled.span`
  font-size: 14px;
  color: gray;
`;

export const ProductCartItem = styled.div`
  margin: 5px 0;
`;
