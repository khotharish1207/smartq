import React, { Component } from "react";
import IconButton from "material-ui/IconButton";
import ContentAdd from "material-ui/svg-icons/content/add";
import ContentRemove from "material-ui/svg-icons/content/remove";
import {
  VegFlag,
  CategoryFoodListItemContainer,
  FoodItemName,
  FoodItemDescription,
  Count,
  ActionButtons,
  Price,
  Description
} from "./CategoryFoodListItemStyles";

const style = {
  border: "1px solid",
  padding: "3px",
  width: "30px",
  height: "30px"
};

export default class CategoryFoodListItem extends Component {
  state = {
    count: 0
  };

  updateFoodCart = () => {
    const { count } = this.state;
    const { name, onUpdateFoodCart, price } = this.props;
    onUpdateFoodCart({ item: name, count, price });
  };

  onRemoveItem = () => {
    const { count } = this.state;
    this.setState({ count: count - 1 }, this.updateFoodCart);
  };

  onAddItem = () => {
    const { count } = this.state;
    this.setState({ count: count + 1 }, this.updateFoodCart);
  };

  render() {
    const { vegflag, name, description, price } = this.props;
    const { count } = this.state;
    return (
      <CategoryFoodListItemContainer>
        <VegFlag veg={vegflag === "veg"} />
        <FoodItemDescription>
          <FoodItemName>{name}</FoodItemName>
          <Description>{description}</Description>
        </FoodItemDescription>
        <ActionButtons>
          <IconButton
            disabled={count === 0}
            style={style}
            onClick={this.onRemoveItem}
          >
            <ContentRemove style={style} />
          </IconButton>
          <Count>{count}</Count>
          <IconButton style={style} onClick={this.onAddItem}>
            <ContentAdd />
          </IconButton>
        </ActionButtons>
        <Price>{`RS ${price}`}</Price>
      </CategoryFoodListItemContainer>
    );
  }
}
