import React from "react";
import styled from "styled-components";
import { MenuItem, Paper } from "material-ui";
import _ from "lodash";


const CategoriesWrapper = styled(Paper)`
  width: 15%;
  float: left;
  display: inline-block;
`;

const Categories = ({ categories }) => {
  return (
    <CategoriesWrapper>
      <MenuItem style={{ background: "#ffcc33" }} primaryText="All" disabled />
      {_.map(categories, (category, index) => (
        <MenuItem key={index} primaryText={category} />
      ))}
    </CategoriesWrapper>
  );
};

export default Categories;
