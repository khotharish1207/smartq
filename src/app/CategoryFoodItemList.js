import React, { Component } from "react";
import styled from "styled-components";
import { MenuItem, Paper } from "material-ui";
import _ from "lodash";
import CategoryFoodListItem from "./CategoryFoodListItem";

const CategoryFoodItemListWrapper = styled(Paper)`
  width: 60%;
  float: left;
  height: 100vh;
  overflow: auto;
  margin: 0 10px;
`;

const CategoryFoodList = ({ title, foodItems = [], onUpdateFoodCart }) => (
  <div>
    <MenuItem style={{ background: "#ffcc33" }} disabled primaryText={title} />
    {_.map(foodItems, (foodItem, idx) => (
      <CategoryFoodListItem
        key={idx}
        {...foodItem}
        onUpdateFoodCart={onUpdateFoodCart}
      />
    ))}
  </div>
);

export default class CategoryFoodItemList extends Component {
  render() {
    const { categoryFood, categories, onUpdateFoodCart } = this.props;
    return (
      <CategoryFoodItemListWrapper id="CategoryFoodItemList">
        {_.map(categories, (category, index) => (
          <CategoryFoodList
            key={index}
            title={category}
            foodItems={categoryFood[category]}
            onUpdateFoodCart={onUpdateFoodCart}
          />
        ))}
      </CategoryFoodItemListWrapper>
    );
  }
}
