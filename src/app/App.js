import React, { Component } from "react";
import { Dialog, AppBar, FlatButton } from "material-ui";
import { connect } from "react-redux";
import styled from "styled-components";
import FoodMarket from "./FoodMarket";
import {
  fetchMarketData,
  updateFoodCart,
  clearFoodCart,
  setData
} from "../redux/actions/actions";

const AppWrapper = styled.div`
  background: #f5f5f5;
`;

class App extends Component {
  componentDidMount() {
    this.props.fetchMarketData();
  }

  onClearFoodCart = () => {
    const { fetchMarketData, clearFoodCart, setData } = this.props;
    clearFoodCart();
    setData([]);
    fetchMarketData();
  };

  render() {
    const {
      foodMarketData = {},
      updateFoodCart,
      foodCart,
      fetchMarketData
    } = this.props;
    return (
      <AppWrapper id="App">
        <AppBar
          title="SmartQ"
          iconElementRight={<label>{"abc.xy.com"}</label>}
          showMenuIconButton={false}
          style={{ background: "#ffcc33" }}
        />
        <FoodMarket
          marketData={foodMarketData}
          onUpdateFoodCart={updateFoodCart}
          cartProducts={foodCart}
          onClearFoodCart={this.onClearFoodCart}
        />
        <Dialog
          title="There is error while fetching data"
          actions={[
            <FlatButton
              label="Retry"
              primary
              onClick={() => {
                fetchMarketData();
              }}
            />
          ]}
          modal={true}
          open={foodMarketData.error}
        />
      </AppWrapper>
    );
  }
}

const mapStateToPros = ({ foodMarketData, foodCart }) => ({
  foodMarketData,
  foodCart
});

const mapDispatchToProps = dispatch => ({
  fetchMarketData: () => dispatch(fetchMarketData()),
  updateFoodCart: item => dispatch(updateFoodCart(item)),
  clearFoodCart: () => dispatch(clearFoodCart()),
  setData: data => dispatch(setData(data))
});

export default connect(mapStateToPros, mapDispatchToProps)(App);
