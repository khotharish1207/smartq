import React from "react";
import _ from "lodash";
import { MenuItem, IconButton, RaisedButton } from "material-ui";
import ActionDelete from "material-ui/svg-icons/action/delete";
import {
  CartContainer,
  SelectedFoodItem,
  Total,
  Count,
  Price,
  ProductCartItem
} from "./CartStyles";

const getTotal = cartProducts =>
  _.reduce(
    cartProducts,
    (total, { count, price }) => {
      total = total + count * price;
      return total;
    },
    0
  );

const Cart = props => (
  <CartContainer>
    <MenuItem
      style={{ background: "#ffcc33" }}
      primaryText="Your Cart"
      disabled
      rightIcon={
        <IconButton
          style={{ margin: "0 12px" }}
          tooltip="Clear cart"
          onClick={() => props.onClearFoodCart()}
        >
          <ActionDelete />
        </IconButton>
      }
    />
    <SelectedFoodItem>
      {_.map(
        _.filter(props.cartProducts, item => item.count !== 0),
        ({ item, count, price }, idx) => (
          <ProductCartItem key={idx}>
            {item}
            <div>
              <Count>{`X ${count}`}</Count>
              <Price>{`RS ${price}`}</Price>
            </div>
          </ProductCartItem>
        )
      )}
    </SelectedFoodItem>
    <Total>
      {"Total "}
      <Price>{`RS ${getTotal(props.cartProducts)}`}</Price>
    </Total>
    <RaisedButton
      label="Checkout"
      backgroundColor="#ffcc33"
      style={{
        width: "80%",
        margin: "10%"
      }}
    />
  </CartContainer>
);

export default Cart;
