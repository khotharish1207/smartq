import styled from "styled-components";

export const VegFlag = styled.div`
  width: 20px;
  height: 20px;
  background-color: ${props => (props.veg ? "green" : "red")};
  float: left;
`;

export const CategoryFoodListItemContainer = styled.div`
  padding: 2%;
  border-bottom: 1px solid #efefef;
  background: #f5f5f5;
  height: 50px;
`;

export const FoodItemName = styled.div``;

export const FoodItemDescription = styled.div`
  margin-left: 10px;
  width: 70%;
  float: left;
`;

export const Count = styled.span`
  vertical-align: super;
  margin: 5px;
`;

export const ActionButtons = styled.div`
  float: left;
`;

export const Price = styled.span`
  margin-left: 10px;
`;

export const Description = styled.div`
  font-size: 14px;
  color: gray;
`;
