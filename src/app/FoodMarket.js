import React, { Component } from "react";
import { Paper } from "material-ui";
import styled from "styled-components";
import Categories from "./Categories";
import CategoryFoodItemList from "./CategoryFoodItemList";
import Cart from "./Cart";

const FoodMarketContainer = styled(Paper)`
  width: 85%;
  margin: 20px auto;
  min-height: 100vh;
`;

export default class FoodMarket extends Component {
  state = {
    categories: []
  };

  componentWillReceiveProps(nextProps) {
    const { marketData } = nextProps;
    if (marketData.data)
      this.setState({ categories: Object.keys(marketData.data) });
  }

  render() {
    const {
      marketData,
      onUpdateFoodCart,
      cartProducts,
      onClearFoodCart
    } = this.props;
    const { categories } = this.state;

    return (
      <FoodMarketContainer id="food-market">
        <Categories categories={categories} />
        <CategoryFoodItemList
          id="CategoryFoodItemList"
          categoryFood={marketData.data || {}}
          categories={categories}
          onUpdateFoodCart={onUpdateFoodCart}
        />
        <Cart cartProducts={cartProducts} onClearFoodCart={onClearFoodCart} />
      </FoodMarketContainer>
    );
  }
}
