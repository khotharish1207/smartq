import axios from "axios";
import { takeEvery, put } from "redux-saga/effects";
import {
  FETCH_DATA,
  setDataStaus,
  setData,
  setError
} from "../actions/actions";

function formatMarketData(data = []) {
  const formatedData = {};
  const categories = data.map(foodItem => foodItem.category);
  categories.map(category => {
    return (formatedData[category] = data.filter(
      foodItem => category === foodItem.category
    ));
  });
  return formatedData;
}

function* fetchMarketData(action) {
  try {
    put(setDataStaus(true));
    put(setError(false));

    const resp = yield axios.get("https://thesmartq.firebaseio.com/menu.json");
    const { data } = resp;

    yield put(setData(formatMarketData(data)));

    put(setDataStaus(false));
  } catch (e) {
    yield put(setDataStaus(false));
    yield put(setError(true));
  }
}

function* rootSaga() {
  yield takeEvery(FETCH_DATA, fetchMarketData);
}

export default rootSaga;
