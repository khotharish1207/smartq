import { createAction } from "redux-actions";

// Food Market Data
export const FETCH_DATA = "FETCH_DATA";
export const fetchMarketData = createAction(FETCH_DATA);

export const SET_DATA_STATUS = "SET_DATA_STATUS";
export const setDataStaus = createAction(SET_DATA_STATUS);

export const SET_DATA = "SET_DATA";
export const setData = createAction(SET_DATA);

export const SET_ERROR = "SET_ERROR";
export const setError = createAction(SET_ERROR);

// Food Product Cart
export const UPDATE_FOOD_CART = "UPDATE_FOOD_CART";
export const updateFoodCart = createAction(UPDATE_FOOD_CART);

export const CLEAR_FOOD_CART = "CLEAR_FOOD_CART";
export const clearFoodCart = createAction(CLEAR_FOOD_CART);