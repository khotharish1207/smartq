import { UPDATE_FOOD_CART, CLEAR_FOOD_CART } from "../actions/actions";

const initialState = {};

const foodCart = (state = initialState, { type, payload }) => {
  switch (type) {
    case UPDATE_FOOD_CART:
      const cartItem = {};
      cartItem[payload.item] = payload;
      return { ...state, ...cartItem };
    case CLEAR_FOOD_CART:
      return initialState;
    default:
      return state;
  }
};

export default foodCart;
