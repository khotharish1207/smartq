import {
  SET_DATA_STATUS,
  SET_DATA,
  SET_ERROR
} from "../actions/actions";

const initialState = { isFetching: false, error: false };

const foodMarketData = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_DATA_STATUS:
      return { ...state, isFetching: payload };
    case SET_DATA:
      return { ...state, isFetching: false, error: false, data: payload };
    case SET_ERROR:
      return { ...state, isFetching: false, error: payload };
    default:
      return state;
  }
};

export default foodMarketData;
