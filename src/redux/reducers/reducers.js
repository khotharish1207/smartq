import { combineReducers } from "redux";
import foodMarketData from "./foodMarketDataReducer";
import foodCart from "./foodCartReducer";

export default combineReducers({
  foodMarketData,
  foodCart
});
