import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";
import reducers from "./reducers/reducers";
import rootSaga from "./sagas/rootSaga";

const composeEnhancers = composeWithDevTools({
  // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});

const configureStore = (initialState = {}) => {
  // create the saga middleware
  const sagaMiddleware = createSagaMiddleware();

  // Create store
  const store = createStore(
    reducers,
    initialState,
    composeEnhancers(applyMiddleware(sagaMiddleware))
  );
  // Run the saga
  sagaMiddleware.run(rootSaga);

  return store;
};

export default configureStore;
