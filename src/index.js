import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import App from "./app/App";
import configureStore from "./redux/store";
import registerServiceWorker from "./registerServiceWorker";
import "./index.css";

const SmartQApp = () => (
  <Provider store={configureStore()}>
    <MuiThemeProvider>
      <App />
    </MuiThemeProvider>
  </Provider>
);

ReactDOM.render(<SmartQApp />, document.getElementById("root"));
registerServiceWorker();
